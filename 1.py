import glob
import torch
import cv2
import threading
from threading import Thread, Lock
import math
import os
from tqdm import trange,tqdm
import shutil

import numpy as np
from torchvision import transforms
cain=torch.jit.load("model.pth")
tran = transforms.ToTensor()
cain.float()
def quantize(img, rgb_range=255):
    return img.mul(255 / rgb_range).clamp(0, 255).round()
for filename in glob.glob('in/*/*'):
    print(filename)
    os.makedirs("o"+filename)
    if os.path.isfile(f"{filename}/im1.webp"):
        im1=cv2.imread(f"{filename}/im1.webp")/255
        im3=cv2.imread(f"{filename}/im3.webp")/255
        shutil.copy(f"{filename}/im1.webp", f"o{filename}/im1.webp")
        shutil.copy(f"{filename}/im3.webp", f"o{filename}/im3.webp")

    else:
        im1=cv2.imread(f"{filename}/frame1.webp")/255
        im3=cv2.imread(f"{filename}/frame3.webp")/255
        shutil.copy(f"{filename}/frame1.webp", f"o{filename}/im1.webp")
        shutil.copy(f"{filename}/frame3.webp", f"o{filename}/im3.webp")

    out=cain(tran(im1).unsqueeze(0).cuda().float(),tran(im3).unsqueeze(0).cuda().float())
    out1=quantize(out[0].data.mul(255))
    out=out1.permute(1, 2, 0).cpu().numpy().astype(np.uint8)
    cv2.imwrite(f"o{filename}/im2.webp", out, [int(cv2.IMWRITE_WEBP_QUALITY), 90])
