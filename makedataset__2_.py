#Copyright 2021 Hubert Sontowski

#Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

#The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import glob
import torch
import cv2
import threading
from threading import Thread, Lock
import math
import os
from tqdm import trange,tqdm
import numpy as np
from torchvision import transforms
cain=torch.jit.load("model.pth")
tran = transforms.ToTensor()
cain.float()
from tqdm import trange
def save(frame1,frame2,frame3,frame,filename):
     cv2.imwrite(f"dataset/{filename}/{frame}/frame1.webp", frame1, [int(cv2.IMWRITE_WEBP_QUALITY), 90])
     cv2.imwrite(f"dataset/{filename}/{frame}/frame2.webp", frame2, [int(cv2.IMWRITE_WEBP_QUALITY), 90])
     cv2.imwrite(f"dataset/{filename}/{frame}/frame3.webp", frame3, [int(cv2.IMWRITE_WEBP_QUALITY), 90])
def calc_psnr(pred, gt, mask=None):
    '''
        Here we assume quantized(0-255) arguments.
    '''
    diff = (pred - gt)
    mse = diff.pow(2).mean() + 1e-8    # mse can (surprisingly!) reach 0, which results in math domain error

    return -10 * math.log10(mse)
def quantize(img, rgb_range=255):
    return img.mul(255 / rgb_range).clamp(0, 255).round()
pbar = tqdm(total=655)
with torch.no_grad():

     for filename in glob.glob('ultimate dataset/*.mkv'):
          pbar.update(1)
          print(filename)
          cap = cv2.VideoCapture(filename)
          ret=True
          frame=0
          while ret==True:
               ret, frame1 = cap.read()

               ret, frame2 = cap.read()
               ret, frame3 = cap.read()
               ret, frame1_ = cap.read()
               _, _ = cap.read()
               ret, frame2_ = cap.read()
               _, _ = cap.read()
               ret, frame3_ = cap.read()
              
               _, _ = cap.read()
               _, _ = cap.read()
               _, _ = cap.read()
               _, _ = cap.read()
               _, _ = cap.read()
               _, _ = cap.read()
               _, _ = cap.read()
               _, _ = cap.read()
               _, _ = cap.read()
               _, _ = cap.read()
               _, _ = cap.read()
               _, _ = cap.read()
               _, _ = cap.read()
               _, _ = cap.read()
               _, _ = cap.read()
               _, _ = cap.read()
               _, _ = cap.read()
               _, _ = cap.read()
               _, _ = cap.read()
               _, _ = cap.read()
               _, _ = cap.read()
               _, _ = cap.read()
               _, _ = cap.read()
               _, _ = cap.read()
               _, _ = cap.read()
               _, _ = cap.read()
               _, _ = cap.read()
               _, _ = cap.read()
               _, _ = cap.read()
               _, _ = cap.read()
               _, _ = cap.read()
               _, _ = cap.read()
               _, _ = cap.read()
               _, _ = cap.read()
               _, _ = cap.read()
               _, _ = cap.read()
               _, _ = cap.read()

               _, _ = cap.read()
               _, _ = cap.read()
               _, _ = cap.read()
               _, _ = cap.read()
               _, _ = cap.read()
               _, _ = cap.read()
               _, _ = cap.read()
               _, _ = cap.read()
               _, _ = cap.read()
               _, _ = cap.read()
               _, _ = cap.read()
               _, _ = cap.read()
               _, _ = cap.read()
               _, _ = cap.read()
               _, _ = cap.read()
               _, _ = cap.read()
               _, _ = cap.read()
               _, _ = cap.read()
               _, _ = cap.read()
               _, _ = cap.read()
               _, _ = cap.read()
               _, _ = cap.read()
               _, _ = cap.read()
               _, _ = cap.read()
               _, _ = cap.read()
               if ret==True:
                    im1 = cv2.resize(frame1, (256,256),  interpolation = cv2.INTER_AREA)
                    im2 = cv2.resize(frame2, (256,256),  interpolation = cv2.INTER_AREA)
                    im3 = cv2.resize(frame3, (256,256),  interpolation = cv2.INTER_AREA)
                    im3 = im3/255
                    im2 = im2/255
                    im1 = im1/255
                    out=cain(tran(im1).unsqueeze(0).cuda().float(),tran(im3).unsqueeze(0).cuda().float())
                    out1=quantize(out[0].data.mul(255))
                    out=out1.permute(1, 2, 0).cpu().numpy().astype(np.uint8)
                    psnr = calc_psnr(tran(out),tran(im2))
                    psnr2 = calc_psnr(tran(im1),tran(im2))
                    psnr3 = calc_psnr(tran(im2),tran(im3))
                    psnr4 = calc_psnr(tran(im1),tran(im3))
                    if psnr2>30:
                         im1 = cv2.resize(frame1_, (256,256),  interpolation = cv2.INTER_AREA)/255
                         im2 = cv2.resize(frame2_, (256,256),  interpolation = cv2.INTER_AREA)/255
                         im3 = cv2.resize(frame3_, (256,256),  interpolation = cv2.INTER_AREA)/255
                    if psnr3>30:
                         im1 = cv2.resize(frame1, (256,256),  interpolation = cv2.INTER_AREA)/255
                         im2 = cv2.resize(frame2_, (256,256),  interpolation = cv2.INTER_AREA)/255
                         im3 = cv2.resize(frame3_, (256,256),  interpolation = cv2.INTER_AREA)/255
                    out=cain(tran(im1).unsqueeze(0).cuda().float(),tran(im3).unsqueeze(0).cuda().float())
                    out=out1.permute(1, 2, 0).cpu().numpy().astype(np.uint8)
                    psnr = calc_psnr(tran(out),tran(im2))
                    if psnr>23 and psnr4<20:
                         frame+=1
                         os.makedirs(f"dataset/{filename}/{frame}")
                         x = threading.Thread(target=save, args=(frame1,frame2,frame3,frame,filename,))
                         x.start()                         

     

               

          frame+=1
